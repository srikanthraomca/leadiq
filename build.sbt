name := "Imgur"

version := "0.1"

scalaVersion := "2.12.7"

val akkaHttpVersion = "10.1.5"
val akkaVersion = "2.5.18"
val scalaAsyncVersion = "0.9.7"
val circeVersion = "0.10.0"
val scalaTestVersion = "3.0.5"
val scalaMockVersion = "3.6.0"

libraryDependencies ++= Seq(
  "org.scala-lang.modules" %% "scala-async" % scalaAsyncVersion,
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,


  "io.circe" %% "circe-generic" % circeVersion, // for auto-derivation of JSON codecs
  "io.circe" %% "circe-parser" % circeVersion, // for parsing
  "org.scalatest" %% "scalatest" % scalaTestVersion % "test",
  "org.scalamock" %% "scalamock-scalatest-support" % scalaMockVersion % "test"

)