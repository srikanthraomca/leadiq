package com.api

import java.io.{PrintWriter, StringWriter}

import akka.http.scaladsl.server.{Directives, ExceptionHandler, Route, RouteConcatenation}
import akka.http.scaladsl.server.Directives._
import com.api.model.ImageController
import com.api.services._
import com.api.util.CirceSupportForAkkaHttp
import io.circe.generic.auto._
import io.circe.syntax._
sealed trait SuperTrait
case class UploadC(urls:List[String])

trait Router extends SuperTrait with RouteConcatenation with CirceSupportForAkkaHttp{
  this: AkkaCoreModule =>

  val exceptionHandler = ExceptionHandler {
    case exception: Exception =>
      val sw = new StringWriter
      exception.printStackTrace(new PrintWriter(sw))
      complete(s"uncaught exception: $sw")
  }

  val jobS:JobService = InMemoryJobService
  val upload:ImageUploader = new ImgurImageUploader
  val download:ImageDownloader = new ImageUrlFileDownloader

  val controller = new ImageController(jobS,upload,download)

  implicit val uploadc = UploadC.asJson

  val routes: Route = pathPrefix("v1"){
    path("images"){
      get{
        controller.getAllLinks
      }
    }~
    pathPrefix("images"){
      path("upload"){
        post{
          entity(as[UploadC]) { param =>
            controller.uploadImages(param.urls)
          }
        }
      }~
      pathPrefix("upload"){
        path(Directives.JavaUUID){
          jobid=> controller.stats(jobid.toString)
        }
      }
    }
  }
}
