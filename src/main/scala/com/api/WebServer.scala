package com.api

import akka.actor.ActorLogging
import akka.http.scaladsl.Http
import com.typesafe.config.ConfigFactory

import scala.async.Async.{async, await}

trait WebServer {
  this: AkkaCoreModule
    with Router =>

  private val config = ConfigFactory.load()
  private val host = config.getString("http.host")
  private val port = config.getInt("http.port")


  private val binding = Http().bindAndHandle(routes, host, port)

  async {
    await(binding)
    println(s"server listening on port $port")
  }
}