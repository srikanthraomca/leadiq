package com.api.util


import java.nio.charset.{Charset, StandardCharsets}
import java.nio.file.Path

import akka.http.scaladsl.HttpExt
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{Accept, RawHeader}
import akka.stream.Materializer
import akka.util.ByteString

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try
import scala.util.control.NonFatal

case class HttpClientResponse(status: StatusCode,
                              body: ByteString,
                              headers: Seq[HttpHeader],
                              contentType: ContentType,
                              charset: Charset) {
  def bodyAsString: String = body.decodeString(charset)
}

case class UnexpectedResponse(response: HttpClientResponse) extends RuntimeException(response.status.toString())

case class HttpClientRequestBuilder(request: HttpRequest) {
  def headers(kvs: (String, String)*): HttpClientRequestBuilder = {
    HttpClientRequestBuilder(request.mapHeaders(_ ++ kvs.map((RawHeader.apply _).tupled)))
  }

  def params(kvs: (String, String)*): HttpClientRequestBuilder = {
    val query = kvs.foldLeft(request.uri.query())((query, curr) => curr +: query)
    HttpClientRequestBuilder(request.withUri(request.uri.withQuery(query)))
  }

  def accept(mediaRanges: MediaRange*): HttpClientRequestBuilder =
    HttpClientRequestBuilder(request.addHeader(Accept(mediaRanges: _*)))

  def acceptJson: HttpClientRequestBuilder =
    HttpClientRequestBuilder(request.addHeader(Accept(MediaRange(MediaTypes.`application/json`))))

  def acceptXml: HttpClientRequestBuilder =
    HttpClientRequestBuilder(request.addHeader(Accept(MediaRange(MediaTypes.`application/xml`))))

  def bodyAsJson(body: String): HttpClientRequestBuilder =
    HttpClientRequestBuilder(request.withEntity(HttpEntity(ContentTypes.`application/json`, body)))

  def bodyAsXml(body: String): HttpClientRequestBuilder =
    HttpClientRequestBuilder(request.withEntity(HttpEntity(ContentTypes.`text/xml(UTF-8)`, body)))

  def bodyAsText(body: String): HttpClientRequestBuilder =
    HttpClientRequestBuilder(request.withEntity(HttpEntity(body)))

  def bodyAsBinary(body: Array[Byte]): HttpClientRequestBuilder =
    HttpClientRequestBuilder(request.withEntity(HttpEntity(body)))

  def bodyAsBinary(body: ByteString): HttpClientRequestBuilder =
    HttpClientRequestBuilder(request.withEntity(HttpEntity(body)))

  def bodyFromFile(contentType: ContentType, file: Path, chunkSize: Int = -1): HttpClientRequestBuilder =
    HttpClientRequestBuilder(request.withEntity(HttpEntity.fromPath(contentType, file, chunkSize)))

  def bodyAsForm(fields: Map[String, String]): HttpClientRequestBuilder =
    HttpClientRequestBuilder(request.withEntity(FormData(fields).toEntity))

  def bodyAsForm(fields: (String, String)*): HttpClientRequestBuilder =
    HttpClientRequestBuilder(request.withEntity(FormData(fields :_*).toEntity))

  def run()(implicit mat: Materializer, http: HttpExt, ec: ExecutionContext): Future[HttpClientResponse] = {
    for {
      response <- http.singleRequest(request)
      contentType = response.entity.contentType
      charset = contentType.charsetOption.map(_.nioCharset()).getOrElse(StandardCharsets.UTF_8)
      body <- response.entity.dataBytes.runFold(ByteString(""))(_ ++ _)
    } yield HttpClientResponse(response.status, body, response.headers, contentType, charset)
  }

  def runTry(failWhen: HttpClientResponse => Boolean = _.status.isFailure())
            (implicit mat: Materializer, http: HttpExt, ec: ExecutionContext): Future[Try[HttpClientResponse]] = {
    run().map {
      case response if failWhen(response) => util.Failure(UnexpectedResponse(response))
      case response => util.Success(response)
    }.recover {
      case NonFatal(cause) => util.Failure(cause)
    }
  }

}

object HttpClientRequestBuilder {

  def get(uri: String): HttpClientRequestBuilder =
    HttpClientRequestBuilder(HttpRequest(uri = uri))

  def head(uri: String): HttpClientRequestBuilder =
    HttpClientRequestBuilder(HttpRequest(method = HttpMethods.HEAD, uri = uri))

  def post(uri: String): HttpClientRequestBuilder =
    HttpClientRequestBuilder(HttpRequest(method = HttpMethods.POST, uri = uri))

  def put(uri: String): HttpClientRequestBuilder =
    HttpClientRequestBuilder(HttpRequest(method = HttpMethods.PUT, uri = uri))

  def patch(uri: String): HttpClientRequestBuilder =
    HttpClientRequestBuilder(HttpRequest(method = HttpMethods.PATCH, uri = uri))

  def delete(uri: String): HttpClientRequestBuilder =
    HttpClientRequestBuilder(HttpRequest(method = HttpMethods.DELETE, uri = uri))

}
