package com.api.model

import com.api.services.{ImageDownloader, ImageUploader, JobService}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.api.AkkaCoreModule
import com.api.util.CirceSupportForAkkaHttp
import io.circe.generic.auto._

import scala.util.{Failure, Success}
class ImageController(jobService: JobService,
                      imgurUploader: ImageUploader,
                      imgDownloader: ImageDownloader) extends CirceSupportForAkkaHttp with AkkaCoreModule{

  def uploadImages(urls: List[String]): Route ={
    val job = jobService.create(urls)
    val jobId = job.jobId
    urls.foreach { url =>
      imgDownloader.download(url)
        .foreach( downloadResult =>
          downloadResult fold(
            error => jobService.reportFailure(jobService.find(jobId.toString), url, error),
            file => imgurUploader.upload(file).onComplete{
              case Success(res)=>
                res match {
                  case Left(error) => jobService.reportFailure(jobService.find(jobId.toString), url, error)
                  case Right(imgurlink) =>jobService.update(jobService.find(jobId.toString), url, imgurlink)
                }
              case Failure(e)=> jobService.reportFailure(jobService.find(jobId.toString), url, e.toString)
            }
          )
        )
    }
    complete(Map("jobId" -> job.jobId.toString))
  }

  def stats(jobId:String):Route={
    jobService.
      find(jobId).
      fold(complete(Map(jobId -> "Not Found"))) { job =>
        complete(job)
      }
  }

  def getAllLinks : Route ={
    val imageImgurUrls = jobService.all.flatMap(
      job => job.images.filter(image => image.uploadUrl.isDefined)
    ).map(_.uploadUrl.get)
    complete(Map("uploaded"->imageImgurUrls))
  }

}
