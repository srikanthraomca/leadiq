package com.api.model

case class Image(uploadUrl: Option[String], downloadUrl: String)
