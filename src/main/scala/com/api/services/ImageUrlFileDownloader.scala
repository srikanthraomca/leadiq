package com.api.services

import java.io.{BufferedOutputStream, File, FileOutputStream}

import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import com.api.AkkaCoreModule
import com.api.util.HttpClientRequestBuilder

import scala.async.Async.{async, await}
import scala.concurrent.Future

trait ImageDownloader {
  def download(url: String): Future[Either[String, File]]
}

class ImageUrlFileDownloader extends ImageDownloader with AkkaCoreModule{

  override def download(url: String): Future[Either[String, File]] = async{


    implicit val http = Http()
    val result = await(HttpClientRequestBuilder.get(url).run())

    result.status match {
      case StatusCodes.OK=>
        val tempFi = File.createTempFile("TES", "")
        val bos = new BufferedOutputStream(new FileOutputStream(tempFi))
        bos.write(result.body.toArray)
        bos.close()
        Right(tempFi)
      case _ =>
        Left(result.bodyAsString)
    }
  }
}
