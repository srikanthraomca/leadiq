package com.api.services

import java.io.File
import java.nio.file.Files

import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import com.api.AkkaCoreModule
import com.api.util.{CirceSupportForAkkaHttp, HttpClientRequestBuilder}
import com.typesafe.config.ConfigFactory
import io.circe.parser._
import io.circe.{HCursor, Json}

import scala.concurrent.Future
import scala.async.Async.{async, await}

trait ImageUploader {
  def upload(image: File): Future[Either[String,String]]
}

class ImgurImageUploader extends ImageUploader with AkkaCoreModule with CirceSupportForAkkaHttp{

  private def readImageToBase64(photo: File): String = {
    val bytes = Files.readAllBytes(photo.toPath)
    javax.xml.bind.DatatypeConverter.printBase64Binary(bytes)
  }

  override def upload(image: File): Future[Either[String, String]] =  async{

    val config = ConfigFactory.load()

    implicit val http = Http()

    var client = HttpClientRequestBuilder.post(config.getString("imgur.uri"))

    client = client.acceptJson

    client = client.headers(("Authorization",s"Client-ID ${config.getString("imgur.clientId")}"))

    val postParams = Map(
      "image" -> readImageToBase64(image),
      "type"->"base64"
    )

    client = client.bodyAsForm(postParams)

    val result = await(client.run())

    result.status match {
      case StatusCodes.OK =>
        Right(getMessage(result.bodyAsString,isErr = true))
      case _ =>
        Left(getMessage(result.bodyAsString,isErr = false))
    }
  }

  def getMessage(response:String,isErr:Boolean):String={
    val doc: Json = parse(response).right.get
    val cursor: HCursor = doc.hcursor
    val data = cursor.downField("data")
    if(isErr)
      data.downField("link").focus.get.asString.get
    else
      data.downField("error").focus.get.toString()

  }

}
