package com.api.services

import com.api.model.{Image, Job, JobStatistics, JobStatus}
import java.time._

trait JobService {
    def create(urls: List[String]): Job
    def all:List[Job]
    def update(job: Job): Unit
    def update(job: Option[Job], downloadUrl: String, imgurUrl: String)
    def reportFailure(job: Option[Job],downloadUrl: String, error: String)
    def find(id: String): Option[Job]
}

object InMemoryJobService extends JobService{

  def getDateWithTime: String = ZonedDateTime.now(ZoneId.of("GMT")).toString

  private var store: Map[String, Job] = Map.empty[String, Job]

  private def calculateStatus(jobStat: JobStatistics): String =
    (jobStat.pending.size, jobStat.completed.size, jobStat.failed.size) match {
      case (pending, _, _) if pending > 0 => JobStatus.InProgress
      case (0, _, _) => JobStatus.Completed
    }

  def getStore: Map[String, Job] = store

  override def create(urls: List[String]): Job = {
    val id = Job.generateId()
    val creationDate = getDateWithTime
    val uploadStat = JobStatistics(
      pending = urls,
      completed = List.empty[String],
      failed = List.empty[String]
    )
    val finished = None
    val images = urls.map(downloadUrl => Image(None, downloadUrl))
    val job = Job(id,creationDate.toString,finished,JobStatus.Pending,images, uploadStat)
    store = store + (job.jobId.toString -> job)
    job
  }
  override def all: List[Job] = store.values.toList

  def find(id: String): Option[Job] = store.get(id)

  override def update(job: Job): Unit = store = store + (job.jobId.toString -> job)

  override def update(jobOpt: Option[Job], downloadUrl: String, imgurUrl: String): Unit = {
    val job = jobOpt.get
    val image = Image(Some(imgurUrl), downloadUrl)
    val newImages = job.images.filter(_.downloadUrl != downloadUrl) :+ image

    val newPending = job.statistic.pending.filter(_ != downloadUrl )
    val finished = if(newPending.isEmpty) Some(getDateWithTime) else None
    val newCompleted = job.statistic.completed :+ imgurUrl

    val newStat = job.statistic.copy(pending = newPending, completed = newCompleted)
    val newStatus = calculateStatus(newStat)
    update(job.copy(finished = finished, images = newImages, statistic = newStat, status = newStatus))
  }

  override def reportFailure(jobOpt: Option[Job], downloadUrl: String, error: String): Unit = {
    val job = jobOpt.get
    val newPending = job.statistic.pending.filter(_ != downloadUrl )
    val finished = if(newPending.isEmpty) Some(getDateWithTime) else None
    val newFailed = job.statistic.failed :+ downloadUrl
    val newStat = job.statistic.copy(pending = newPending, failed = newFailed)
    val newStatus = calculateStatus(newStat)

    update(job.copy(finished= finished, status = newStatus, statistic = newStat))
  }
}