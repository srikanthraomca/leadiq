package com.api.service

import com.api.model.{Image, Job, JobStatistics, JobStatus}
import com.api.services.InMemoryJobService
import org.scalatest.{ Matchers, WordSpec}
import com.api.services.InMemoryJobService._

class JobSpec extends WordSpec with Matchers {

  val job = Job(
    Job.generateId(), getDateWithTime, None, JobStatus.InProgress, List(Image(None, "link1"), Image(None, "link2")), JobStatistics(
      pending = List("link"),
      completed = List.empty[String],
      failed = List.empty[String]
    )
  )

  "createJob" should{
    "initialised job and return jobId " in {
      val urls = List("link1", "link2")
      val e_job = Job(
        Job.generateId(), getDateWithTime, None, JobStatus.InProgress, List(Image(None, "link1"), Image(None, "link2")), JobStatistics(
          pending = urls,
          completed = List.empty[String],
          failed = List.empty[String]
        )
      )
      InMemoryJobService.create(urls).images shouldBe e_job.images
    }

    "after creating job store size will not be zero" in{
      InMemoryJobService.getStore.size shouldNot be (0)
    }
  }

  "updateJob" should{
    "update Store" in{
      InMemoryJobService.update(job)
      val store = InMemoryJobService.getStore

      store.get(job.jobId.toString) shouldBe Some(job)
    }
    "update link if link is processed "in{

      val updatedLink = Image(Some("updateLink"), "link1")
      InMemoryJobService.update(Some(job),updatedLink.downloadUrl,updatedLink.uploadUrl.get)
      val afterUpdate = InMemoryJobService.getStore (job.jobId.toString).images.filter(i => i.downloadUrl == updatedLink.downloadUrl).head

      afterUpdate.uploadUrl shouldBe updatedLink.uploadUrl

    }
  }
  "reportFailure" should{
    "should update the store" in {
      val updatedLink = Image(None, "link1")
      InMemoryJobService.reportFailure(Some(job), updatedLink.downloadUrl, "error")
      val afterUpdate = InMemoryJobService.getStore (job.jobId.toString).statistic.failed.head

      afterUpdate shouldBe updatedLink.downloadUrl
    }
  }

  "Job details" should {
    "return None if jobId not present" in{
      val jobId = "jobIdThatIsNotPresent"
      InMemoryJobService.find(jobId) shouldBe None
    }

    "return job details if jobId present" in{
      InMemoryJobService.update(job)
      InMemoryJobService.find(job.jobId.toString) shouldBe Some(job)
    }
  }

  "getAll" should{
    "should return number of jobs greater than zero" in{
      InMemoryJobService.update(job)

      InMemoryJobService.all.size shouldNot be (0)
    }
  }

}
