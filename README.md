##imgur

### Used packages
```
1. scala, scalatest
2. Akka,Circle-json
```
### run
```
sbt run
```
### ImageUpload
```
> http://localhost:9000/v1/images/upload

```

### Get upload job status
```
> http://localhost:9000/v1/images/upload/jobId

```

### Get list of all uploaded images
```
> http://localhost:9000/v1/images
```

### running on docker
```
    sudo docker build -t srikanthrao:app .
    sudo docker run -i -t srikanthrao:app /bin/bash
    sudo docker run -i -p 8080:8080 srikanthrao:app sbt run
```